package hr.apps.mosaic.example_93;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;

/**
 * Created by Stray on 19.9.2017..
 */

public class Preferences {

	public static String PREFS_FILE = "MyPreferences";
	public static String PREFS_KEY_USERNAME = "username";
	public static String PREFS_KEY_COLOR = "color";

	private SharedPreferences mSharedPreferences;

	public Preferences(Context context){
		context = context.getApplicationContext();
		this.mSharedPreferences =
				context.getSharedPreferences(PREFS_FILE, context.MODE_PRIVATE);
	}

	public void saveUsername(String username) {
		SharedPreferences.Editor editor = this.mSharedPreferences.edit();
		editor.putString(PREFS_KEY_USERNAME, username);
		editor.apply();
	}

	public void saveColor(int color){
		SharedPreferences.Editor editor = this.mSharedPreferences.edit();
		editor.putInt(PREFS_KEY_COLOR, color);
		editor.apply();
	}

	public int retrieveColor(){
		return this.mSharedPreferences.getInt(PREFS_KEY_COLOR,0);
	}

	public String retrieveUsername() {
		return this.mSharedPreferences.getString(PREFS_KEY_USERNAME,"Unknown");
	}
}
