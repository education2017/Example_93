package hr.apps.mosaic.example_93;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends Activity {

	Random mRandom = new Random();

	@BindView(R.id.tvMessage) TextView tvMessage;
	@BindView(R.id.rlRoot) RelativeLayout rlRoot;
	@BindView(R.id.bStoreData) Button bStoreData;
	@BindView(R.id.etName) EditText etName;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		ButterKnife.bind(this);
		this.presentData();
	}

	private void presentData() {
		Preferences preferences = new Preferences(this);
		int color = preferences.retrieveColor();
		String name = preferences.retrieveUsername();

		this.tvMessage.setText("Welcome back " + name);
		this.rlRoot.setBackgroundColor(color);
	}

	@OnClick(R.id.bStoreData)
	public void onClick(){
		String name = this.etName.getText().toString();
		int r = this.mRandom.nextInt(256);
		int g = this.mRandom.nextInt(256);
		int b = this.mRandom.nextInt(256);
		int color = Color.argb(255,r,g,b);
		Preferences preferences = new Preferences(this);
		preferences.saveUsername(name);
		preferences.saveColor(color);
		this.presentData();
	}
}
